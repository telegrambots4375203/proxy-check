from pyrogram import Client, filters
import requests
from concurrent.futures import ThreadPoolExecutor
from tabulate import tabulate

API_ID = 10471716
API_HASH = 'f8a1b21a13af154596e2ff5bed164860'
BOT_TOKEN = '6458881041:AAGB4N0VcQXy84aNaCF25ojDyBbazvxdb3Y'

app = Client('proxy_bot', api_id=API_ID, api_hash=API_HASH, bot_token=BOT_TOKEN)

def determine_proxy_type(proxy):
    parsed_proxy = urlparse(proxy)
    if parsed_proxy.scheme:
        return parsed_proxy.scheme.lower()
    else:
        # Default to HTTP if the scheme is not specified
        return "http"

def check_proxy(proxy):
    proxy_type = determine_proxy_type(proxy)
    
    try:
        if proxy_type == "http":
            proxies = {proxy_type: f"{proxy_type}://{proxy}"}
        elif proxy_type == "socks5":
            proxies = {"http": f"socks5://{proxy}", "https": f"socks5://{proxy}"}
        else:
            return proxy, False
        
        response = requests.get("https://www.google.com", proxies=proxies, timeout=5)
        
        if response.status_code == 200:
            return proxy, True
    except:
        pass
    
    return proxy, False

def log_progress(message):
    print(f"[INFO] {message}")

def log_error(error_message):
    print(f"[ERROR] {error_message}")

@app.on_message(filters.command("start"))
def start(client, message):
    client.send_message(message.chat.id, "Welcome! This bot checks the type and liveliness of proxies. Send a .txt file with proxy list as IP:Port in each line.")

@app.on_message(filters.command("help"))
def help_command(client, message):
    help_text = (
        "Commands:\n"
        "/start - Start the bot\n"
        "/help - Show this help message\n"
        "/check - Check proxies from a .txt file"
    )
    client.send_message(message.chat.id, help_text)

@app.on_message(filters.command("check"))
def check_proxies_command(client, message):
    if not message.document:
        client.send_message(message.chat.id, "Please send a .txt file with proxy list to check.")
        return

    log_progress("Proxy checking process started.")

    file_id = message.document.file_id
    file_info = app.get_file(file_id)
    file_path = file_info.file_path

    proxies = []

    with app.download_media(file_path) as file:
        with open(file, "r") as f:
            proxies = f.read().splitlines()

    working_proxies = []
    non_working_proxies = []

    log_progress(f"Total proxies to check: {len(proxies)}")

    with ThreadPoolExecutor(max_workers=10) as executor:
        futures = [executor.submit(check_proxy, proxy) for proxy in proxies]
        for future in futures:
            results = future.result()
            log_progress(f"Checked Proxy: {results[0]}, Status: {'Working' if results[1] else 'Not Working'}")
            if results[1]:
                working_proxies.append(results[0])
            else:
                non_working_proxies.append(results[0])

    working_table = tabulate(working_proxies, headers=["Working Proxies"], tablefmt="pretty")
    non_working_table = tabulate([proxy] for proxy in non_working_proxies, headers=["Non-working Proxies"], tablefmt="pretty")

    result_message = f"Working Proxies:\n{working_table}\n\nNon-working Proxies:\n{non_working_table}"

    with open("proxy_check_result.txt", "w") as result_file:
        result_file.write(result_message)

    log_progress("Proxy checking process completed.")
    client.send_document(message.chat.id, document="proxy_check_result.txt")

@app.on_message(filters.command("unknown_command"))
def unknown_command(client, message):
    client.send_message(message.chat.id, "Sorry, I don't understand that command. Use /help for a list of available commands.")

@app.on_message(filters.command("error"))
def error_command(client, message):
    log_error("Simulating an error for demonstration purposes.")
    # Simulating an error for demonstration purposes
    raise Exception("This is a sample error message.")

@app.on_message(filters.command("stop"))
def stop_command(client, message):
    # Gracefully stop the bot
    client.stop()

@app.on_message(filters.command("ping"))
def ping_command(client, message):
    client.send_message(message.chat.id, "Pong!")

@app.on_error()
def error_handler(client, message):
    log_error("An unexpected error occurred. Please try again later.")
    client.send_message(message.chat.id, "An unexpected error occurred. Please try again later.")

app.run()
